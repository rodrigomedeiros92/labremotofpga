----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07.06.2018 17:16:52
-- Design Name: 
-- Module Name: top_wrapper - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_wrapper is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           rx : in STD_LOGIC;
           tx : out STD_LOGIC;
           an : out STD_LOGIC_VECTOR (7 downto 0);
           seg : out STD_LOGIC_VECTOR (6 downto 0);
           dp : out STD_LOGIC;
           led : out STD_LOGIC_VECTOR (15 downto 0));
end top_wrapper;

architecture Behavioral of top_wrapper is

component serialcom is
port(reset		  :  in std_logic;
	  clk 		  :  in std_logic;
	  start   	  :  in std_logic;
      data_in     :  in std_logic_vector(7 downto 0);
	  rx    	  :  in std_logic;
	  data_out    : out std_logic_vector(7 downto 0);
	  tx_ready    : out std_logic;
	  tx          : out std_logic);
end component serialcom;

component deco_leds_7seg is
    Port ( clk : in STD_LOGIC;  
       reset : in STD_LOGIC;
       tx_ready : in STD_LOGIC;
       led : in STD_LOGIC_VECTOR (15 downto 0);
       seg : in STD_LOGIC_VECTOR (6 downto 0);
       dp : in STD_LOGIC;
       an : in STD_LOGIC_VECTOR (7 downto 0);
       start : out STD_LOGIC;
       data_in : out STD_LOGIC_VECTOR (7 downto 0));
end component deco_leds_7seg;
 

component deco_sw_bts is
    Port (clk : in STD_LOGIC;
       data_out : in STD_LOGIC_VECTOR (7 downto 0);
       reset: in STD_LOGIC;
       sw : out STD_LOGIC_VECTOR (15 downto 0);
       buttons : out STD_LOGIC_VECTOR (4 downto 0));
end component deco_sw_bts;


component user is
Port ( clk : in STD_LOGIC;
       reset : in STD_LOGIC; 
       sw : in STD_LOGIC_VECTOR (15 downto 0);
       buttons : in STD_LOGIC_VECTOR(3 downto 0);
       led : out STD_LOGIC_VECTOR (15 downto 0);
       seg : out STD_LOGIC_VECTOR (6 downto 0);
       an : out STD_LOGIC_VECTOR (3 downto 0);
       dp: out STD_LOGIC);
end component user;

signal s_start : std_logic:='0';
signal s_sw : std_logic_vector(15 downto 0) :=(others=>'0');
signal s_buttons : std_logic_vector(4 downto 0) :=(others=>'0');
signal s_tx : std_logic:='0';
signal s_data_in : std_logic_vector(7 downto 0) :=(others=>'0');
signal s_led : std_logic_vector(15 downto 0) :=(others=>'0');
signal s_seg : std_logic_vector(6 downto 0) :=(others=>'0');
signal s_dp : std_logic:='0';
signal s_an : std_logic_vector(7 downto 0) :=(others=>'0');
signal s_an_user: std_logic_vector(3 downto 0) :=(others=>'0');
signal s1_data_in : std_logic_vector(7 downto 0) :=(others=>'0');
signal s_tx_ready : std_logic:='0';
signal s_din : std_logic:='0';
signal s_data_out : std_logic_vector(7 downto 0) :=(others=>'0');


begin

led<=s_led;
s_an<=s_an_user&"1111";
an<=s_an;
dp<=s_dp;
seg<=s_seg;
tx<=s_tx;



u0: component serialcom
port map(reset=>reset,
         clk=>clk,
         start=>s_start,
         data_in=>s1_data_in,
         tx_ready=>s_tx_ready,   
         rx=>rx,
         data_out=>s_data_out,
         tx=>s_tx 
    );

u1: component deco_leds_7seg
port map(clk=>clk,
         reset=>reset,
         tx_ready=>s_tx_ready,
         led=>s_led,
         seg=>s_seg,
         dp=>s_dp,
         an=>s_an,
         start=>s_start,
         data_in=>s1_data_in
    );


u2: component deco_sw_bts
port map(
         clk=>clk,   
         data_out=>s_data_out,
         reset=>reset,
         sw=>s_sw,
         buttons=>s_buttons
    );

u3: component user
port map(clk=>clk,
         reset=>s_buttons(0),
         sw=>s_sw,
         buttons=>s_buttons(4 downto 1),
         led=>s_led,
         seg=>s_seg,
         an=>s_an_user,
         dp=>s_dp
    );
    
    
 end Behavioral;

