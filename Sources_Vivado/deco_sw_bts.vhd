----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07.06.2018 16:29:47
-- Design Name: 
-- Module Name: user - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity deco_sw_bts is
    Port ( clk: in STD_LOGIC;
           data_out : in STD_LOGIC_VECTOR (7 downto 0);
           reset: in STD_LOGIC;
           sw : out STD_LOGIC_VECTOR (15 downto 0);
           buttons : out STD_LOGIC_VECTOR (4 downto 0));
end deco_sw_bts;

architecture Behavioral of deco_sw_bts is
--sinais para os niveis logicos dos botoes e dos switches
signal toggle_buttons: std_logic_vector(4 downto 0):=(others =>'0');
signal toggle_sw: std_logic_vector(15 downto 0):=(others =>'0');
signal s_high: std_logic:='1';
signal s_low: std_logic:='0';
begin
   
    process(clk,data_out,reset)
        begin
             if rising_edge(clk) then
                if reset='1' then
                toggle_sw<=(others =>'0');
                else
                    case (not data_out) is -- Recebe not ASCII , pois o serialcom é projeto com entradas not 
                        -- Decodoficador para os sw ir para nível lógico baixo
                        when "01100001"=>toggle_sw(0)<=s_low;  --a
                        when "01100010"=>toggle_sw(1)<=s_low;  --b
                        when "01100011"=>toggle_sw(2)<=s_low;  --c
                        when "01100100"=>toggle_sw(3)<=s_low;  --d
                        when "01100101"=>toggle_sw(4)<=s_low;  --e
                        when "01100110"=>toggle_sw(5)<=s_low;  --f
                        when "01100111"=>toggle_sw(6)<=s_low;  --g
                        when "01101000"=>toggle_sw(7)<=s_low;  --h 
                        when "01101001"=>toggle_sw(8)<=s_low;  --i
                        when "01101010"=>toggle_sw(9)<=s_low;  --j
                        when "01101011"=>toggle_sw(10)<=s_low; --k
                        when "01101100"=>toggle_sw(11)<=s_low; --l
                        when "01101101"=>toggle_sw(12)<=s_low; --m
                        when "01101110"=>toggle_sw(13)<=s_low; --n
                        when "01101111"=>toggle_sw(14)<=s_low; --o
                        when "01110000"=>toggle_sw(15)<=s_low; --p
                        -- Decodoficador para os sw ir para nível lógico alto
                        when "01000001"=>toggle_sw(0)<=s_high; --A
                        when "01000010"=>toggle_sw(1)<=s_high; --B
                        when "01000011"=>toggle_sw(2)<=s_high; --C
                        when "01000100"=>toggle_sw(3)<=s_high; --D
                        when "01000101"=>toggle_sw(4)<=s_high; --E
                        when "01000110"=>toggle_sw(5)<=s_high; --F
                        when "01000111"=>toggle_sw(6)<=s_high; --G
                        when "01001000"=>toggle_sw(7)<=s_high; --H
                        when "01001001"=>toggle_sw(8)<=s_high; --I
                        when "01001010"=>toggle_sw(9)<=s_high; --J
                        when "01001011"=>toggle_sw(10)<=s_high; --K
                        when "01001100"=>toggle_sw(11)<=s_high; --L
                        when "01001101"=>toggle_sw(12)<=s_high; --M
                        when "01001110"=>toggle_sw(13)<=s_high; --N 
                        when "01001111"=>toggle_sw(14)<=s_high; --O
                        when "01010000"=>toggle_sw(15)<=s_high; --P
                        -- Decodificador para os buttons ir para nível lógico baixo
                        when "01110001"=>toggle_buttons(0)<=s_low; --q
                        when "01110010"=>toggle_buttons(1)<=s_low; --r
                        when "01110011"=>toggle_buttons(2)<=s_low; --s
                        when "01110100"=>toggle_buttons(3)<=s_low; --t
                        when "01110101"=>toggle_buttons(4)<=s_low; --u
                      -- Decodificador para os buttons ir para nível lógico alto
                        when "01010001"=>toggle_buttons(0)<=s_high; --Q
                        when "01010010"=>toggle_buttons(1)<=s_high; --R
                        when "01010011"=>toggle_buttons(2)<=s_high; --S
                        when "01010100"=>toggle_buttons(3)<=s_high; --T
                        when "01010101"=>toggle_buttons(4)<=s_high; --U                                         
                        when others => toggle_sw <= toggle_sw;
                    end case;
                end if;
             end if;                                 
        end process;
buttons<=toggle_buttons;    
sw<=toggle_sw;
end Behavioral;



